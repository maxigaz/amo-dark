# Changelog

Changes beyond v1.2.0 are summarised here. For older changes, see the Tags page on [Codeberg](https://codeberg.org/maxigaz/amo-dark/tags) or [GitLab](https://gitlab.com/maxigaz/amo-dark/-/tags).

## v1.2.1

- Fixed gradient at the bottom of long reviews on reviews page
- Fixed badges with the label 'Recommended'
- Styled blog posts

## v1.2.0

- Fixed general background being white
- Main page: Fixed text and icon colour and card background
- Add-on page:
	- Fixed recommended add-on badge
	- Fixed colour of rating stars
- Reviews page:
	- Made stars yellow
	- Fixed text colour on select button “Show all reviews“
