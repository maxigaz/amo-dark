# Mozilla Add-ons Dark Theme

A dark theme for the website [Mozilla Add-ons](https://addons.mozilla.org) (aka AMO), written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format.

This style has been written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), although it might be compatible with other style managers such as [xStyle](https://github.com/FirefoxBar/xStyle).

## Screenshots

![A screenshot showing the main page](screenshot01.png)

![A screenshot showing the page of an extension](screenshot02.png)

## Prerequisites

First of all, **you need a style manager**. The style has been tested with [Stylus](https://add0n.com/stylus.html), although it is also compatible with [xStyle](https://github.com/FirefoxBar/xStyle). Other style managers might also work but are *not* supported.

Then, **you need Firefox** (other web browsers based on Firefox might work, but are not tested) and **tweak the settings of Firefox**. The following can also be read in Stylus when you navigate to AMO and bring up the panel of Stylus; I'm including a paraphrased version here for reference:

Open `about:config` and do the following:

1. Add a new boolean (with the right click context menu) with the name `privacy.resistFingerprinting.block_mozAddonManager` and set its value to `true`.
	* This will disable all “special” AMO privileges such as determining which add-ons are already installed or uninstalling add-ons from the website.
2. Remove the substring `addons.mozilla.org,` from `extensions.webextensions.restrictedDomains`.
	* This will allow all currently installed WebExtensions to manipulate the contents of AMO websites and APIs.

## Install

Install the stylesheet by clicking on one of the mirror links below and a new window of Stylus should open, asking you to confirm to install the style.

Mirrors:

- Codeberg: [Install directly with Stylus](https://codeberg.org/maxigaz/amo-dark/raw/master/mozilla-addons-dark.user.css)
- GitLab: [Install directly with Stylus](https://gitlab.com/maxigaz/amo-dark/raw/master/mozilla-addons-dark.user.css)
- UserStyles.world [Install directly with Stylus](https://userstyles.world/api/style/6461.user.css)

## Report Issues

If you have any issues with the stylesheet, please, report them to any of the following issue trackers:

- [Codeberg Issues](https://codeberg.org/maxigaz/amo-dark)
- [GitLab Issues](https://gitlab.com/maxigaz/amo-dark)

## More userstyles from me

If you like this userstyle, check out the list of all styles created/maintained by me [here](https://gitlab.com/maxigaz/userstyles).
